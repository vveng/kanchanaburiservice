package city.smart.kanchanaburiservice.utils;

import android.content.Intent;
import android.net.Uri;
import android.util.Log;

import com.google.android.gms.wearable.DataEvent;
import com.google.android.gms.wearable.DataEventBuffer;
import com.google.android.gms.wearable.DataMap;
import com.google.android.gms.wearable.DataMapItem;
import com.google.android.gms.wearable.MessageEvent;
import com.google.android.gms.wearable.WearableListenerService;

import java.util.ArrayList;

public class ListenerService extends WearableListenerService {
    private static final String TAG = ListenerService.class.getSimpleName();
    @Override
    public void onDataChanged(DataEventBuffer dataEvents) {
        Log.d(TAG, "onDataChanged: " + dataEvents);

        for (DataEvent event : dataEvents) {
            if (event.getType() == DataEvent.TYPE_CHANGED
                    && event.getDataItem() != null
                    && Constants.ATTRACTION_PATH.equals(event.getDataItem().getUri().getPath())) {

                DataMapItem dataMapItem = DataMapItem.fromDataItem(event.getDataItem());
                ArrayList<DataMap> attractionsData =
                        dataMapItem.getDataMap().getDataMapArrayList(Constants.EXTRA_ATTRACTIONS);
                showNotification(dataMapItem.getUri(), attractionsData);
            }
        }
    }//on onDataChanged


    @Override
    public void onMessageReceived(MessageEvent messageEvent) {
        Log.v(TAG, "onMessageReceived: " + messageEvent);

        if (Constants.CLEAR_NOTIFICATIONS_PATH.equals(messageEvent.getPath())) {
            // Request for this device to clear its notifications
            UtilityService.clearNotification(this);
        } else if (Constants.START_ATTRACTION_PATH.equals(messageEvent.getPath())) {
            // Request for this device open the attraction detail screen
            // to a specific tourist attraction
//            String attractionName = new String(messageEvent.getData());
//            //เรียกหน้าจอแสดงรายละเอียดสานที่น่าสน
              //bug
//            Intent intent = DetailActivity.getLaunchIntent(this, attractionName);
//            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//            startActivity(intent);
        } else if (Constants.START_NAVIGATION_PATH.equals(messageEvent.getPath())) {
            // Request for this device to start Maps walking navigation to
            // specific tourist attraction
            String attractionQuery = new String(messageEvent.getData());
            Uri uri = Uri.parse(Constants.MAPS_NAVIGATION_INTENT_URI + Uri.encode(attractionQuery));
            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }
    }//onMessageReceived


    private void showNotification(Uri attractionsUri, ArrayList<DataMap> attractions) {

    }
}
