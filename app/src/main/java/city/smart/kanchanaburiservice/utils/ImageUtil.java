/*
 * Copyright 2017 Rozdoum
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package city.smart.kanchanaburiservice.utils;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.ImageView;

import com.bumptech.glide.RequestManager;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.SimpleTarget;
import city.smart.kanchanaburiservice.R;

import java.util.Date;


public class ImageUtil {

    public static final String TAG = ImageUtil.class.getSimpleName();

    public static String generateImageTitle(String parentId) {
        String kan = "Kanchanaburi";
        if (parentId != null) {
            return  kan+ parentId;
        }

        return kan + new Date().getTime();
    }

    public static void loadImage(RequestManager glideRequests, String url, ImageView imageView) {
        loadImage(glideRequests, url, imageView, DiskCacheStrategy.ALL);
    }

    public static void loadImage(RequestManager glideRequests, String url, ImageView imageView, DiskCacheStrategy diskCacheStrategy) {
        glideRequests.load(url)
                .apply(new RequestOptions()
                        .diskCacheStrategy(DiskCacheStrategy.DATA)
                        .error(R.drawable.empty_photo)
                        .centerCrop()
                )
                .into(imageView);
    }

    public static void loadImage(RequestManager glideRequests, String url, ImageView imageView,
                                 RequestListener<Drawable> listener) {
        glideRequests.load(url)
                .apply(new RequestOptions()
                        .diskCacheStrategy(DiskCacheStrategy.DATA)
                        .error(R.drawable.empty_photo)
                        .centerCrop()
                )
                .listener(listener)
                .into(imageView);
    }

    public static void loadImageCenterCrop(RequestManager glideRequests, String url, ImageView imageView,
                                           int width, int height) {
        glideRequests.load(url)
                .apply(new RequestOptions()
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .error(R.drawable.empty_photo)
                        .centerCrop()
                        .override(width, height)
                )
                .into(imageView);
    }

    public static void loadImageCenterCrop(RequestManager glideRequests, String url, ImageView imageView,
                                           int width, int height, RequestListener<Drawable> listener) {
        glideRequests.load(url)
                .apply(new RequestOptions()
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .error(R.drawable.empty_photo)
                        .centerCrop()
                        .override(width, height)
                )
                .listener(listener)
                .into(imageView);
    }

    public static void loadImageCenterCrop(RequestManager glideRequests, String url, ImageView imageView,
                                           RequestListener<Drawable> listener) {
        glideRequests.load(url)
                .apply(new RequestOptions()
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .placeholder(R.drawable.empty_photo)
                        .centerCrop()
                        .fitCenter()
                )
                .listener(listener)
                .into(imageView);
    }


    @Nullable
    public static Bitmap loadBitmap(RequestManager glideRequests, String url, int width, int height) {
        try {
            return glideRequests.asBitmap()
                    .load(url)
                    .apply(new RequestOptions()
                            .diskCacheStrategy(DiskCacheStrategy.DATA)
                            .placeholder(R.drawable.empty_photo)
                            .centerCrop()
                            .fitCenter()
                    )
                    .submit(width, height)
                    .get();
        } catch (Exception e) {
            Log.e(TAG, "getBitmapfromUrl", e);
            return null;
        }
    }

    public static void loadImageWithSimpleTarget(RequestManager glideRequests, String url, SimpleTarget<Bitmap> simpleTarget) {
        glideRequests
                .asBitmap()
                .load(url)
                .apply(new RequestOptions()
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .placeholder(R.drawable.empty_photo)
                        .fitCenter()
                )
                .into(simpleTarget);
    }

    public static void loadLocalImage(RequestManager glideRequests, Uri uri, ImageView imageView,
                                      RequestListener<Drawable> listener) {
        glideRequests
                .load(uri)
                .apply(new RequestOptions()
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .placeholder(R.drawable.empty_photo)
                        .skipMemoryCache(true)
                        .fitCenter()
                )
                .listener(listener)
                .into(imageView);
    }
}
