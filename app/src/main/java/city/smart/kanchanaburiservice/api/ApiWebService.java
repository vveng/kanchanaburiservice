package city.smart.kanchanaburiservice.api;

import city.smart.kanchanaburiservice.database.entity.Attraction;
import city.smart.kanchanaburiservice.database.entity.Contacts;
import city.smart.kanchanaburiservice.database.entity.News;

import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiWebService {
    @GET("attaction")
    Call<List<Attraction>> getAttaction(@Query("page") int page, @Query("start") int start, @Query("limit") int limit);

    @GET("attaction/{id}")
    Call<Attraction> getAttactionByID(@Path("id") int id);

    @GET("news")
    Call<List<News>> getNews(@Query("page") int page, @Query("start") int start, @Query("limit") int limit);

    @GET("concacts")
    Call<List<Contacts>> getContacs(@Query("page") int page, @Query("start") int start, @Query("limit") int limit);

    @FormUrlEncoded
    @POST("complaint")
    Call<Boolean> savePost(@FieldMap Map<String, String> field);
}
