package city.smart.kanchanaburiservice.database.entity;

import android.arch.persistence.room.Entity;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.HashMap;
import java.util.Map;

@Entity
public class Complain implements Parcelable {

    private int  _id;

    @SerializedName("title")
    @Expose
    private String _title;

    @SerializedName("desc")
    @Expose
    public String _description;

    @SerializedName("complaint_type_id")
    @Expose
    private String _complainType;

    @SerializedName("reported_name")
    @Expose
    private String _name;

    @SerializedName("reported_phone")
    @Expose
    private String _phone;

//    @SerializedName("image")
//    @Expose
    private Uri _image;

    /* getter and setter */
    public String get_complainType() {
        return _complainType;
    }
    public void set_complainType(String _complainType) {
        this._complainType = _complainType;
    }
    public int get_id() {
        return _id;
    }
    public void set_id(int _id) {
        this._id = _id;
    }
    public void set_phone(String _phone) {
        this._phone = _phone;
    }
    public String get_phone() {
        return _phone;
    }
    public String get_name() {
        return _name;
    }
    public void set_name(String _name) {
        this._name = _name;
    }
    public String get_title() {
        return _title;
    }
    public void set_title(String _title) {
        this._title = _title;
    }
    public String get_description() {
        return _description;
    }
    public void set_description(String _description) {
        this._description = _description;
    }
    public Uri get_image() {
        return _image;
    }
    public void set_image(Uri _image) {
        this._image = _image;
    }

    public Complain(){}
    public Complain(String name, String title, String phone, String description, String image) {
        set_name(name);
        set_title(title);
        set_description(description);
        set_image(Uri.parse(image));
        set_phone(phone);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(get_title());
        parcel.writeString(get_description());
        parcel.writeParcelable(get_image(),0);
        parcel.writeString(get_name());
        parcel.writeString(get_phone());
    }
    /**
     * Retrieving Movie data from Parcel object
     * This constructor is invoked by the method createFromParcel(Parcel source) of
     * the object CREATOR
     **/
    private Complain(Parcel in){
        set_title(in.readString());
        set_description(in.readString());
        set_image((Uri) in.readParcelable(null));
        set_name(in.readString());
        set_phone(in.readString());
    }
    public Map buildDefaultParams(){
        Map defaults = new HashMap();
        defaults.put("title", get_title());
        defaults.put("desc", get_description());
        defaults.put("complaint_type_id", get_complainType());
        defaults.put("reported_name", get_name());
        defaults.put("reported_phone", get_phone());
        defaults.put("is_solved", "0");
        defaults.put("id", "0");
        return defaults;
    }
    public static final Creator<Complain> CREATOR = new Creator<Complain>() {
        @Override
        public Complain createFromParcel(Parcel source) {
            return new Complain(source);
        }

        @Override
        public Complain[] newArray(int size) {
            return new Complain[size];
        }
    };
}
