package city.smart.kanchanaburiservice.database.entity;


import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * A simple shared tourist attraction class to easily pass data around. Used
 * in both the mobile app and wearable app.
 */
@Entity
public class Attraction implements Parcelable {


    @PrimaryKey
    @NonNull
    @SerializedName("id")
    @Expose
    private int  _id;


    @SerializedName("name")
    @Expose
    public String name;

    @SerializedName("description")
    @Expose
    public String description;

    @SerializedName("image")
    @Expose
    private String _image;

    @SerializedName("lat")
    @Expose
    private double _lat;

    @SerializedName("lon")
    @Expose
    private double _lng;


    public String longDescription;

    public Uri imageUrl;
    public Uri secondaryImageUrl;
    public LatLng location;
    public String city;

    public Bitmap picture;
    public Bitmap secondaryImage;
    public String distance;

    @NonNull
    public int get_id() {
        return _id;
    }

    public void set_id(@NonNull int _id) {
        this._id = _id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String get_image() {
        return _image;
    }

    public void set_image(String _image) {
        this._image = _image;
    }

    public double get_lat() {
        return _lat;
    }

    public void set_lat(double _lat) {
        this._lat = _lat;
    }

    public double get_lng() {
        return _lng;
    }

    public void set_lng(double _lng) {
        this._lng = _lng;
    }

    public Attraction() {
    }
    public Attraction(String name, String description, String longDescription, Uri imageUrl,
                      Uri secondaryImageUrl, LatLng location, String city) {
        this.name = name;
        this.description = description;
        this.longDescription = longDescription;
        this.imageUrl = imageUrl;
        this.secondaryImageUrl = secondaryImageUrl;
        this.location = location;
        set_lat(this.location.latitude);
        set_lng(this.location.longitude);
        this.city = city;
    }

    public Attraction(@NonNull int id, String name, String description, String _image, double lat, double lng) {
        this._id = id;
        this.name = name;
        this.description = description;
        //this.longDescription = longDescription;
        this.imageUrl = Uri.parse(_image);
        this._image = _image;
        ///this.secondaryImageUrl = secondaryImageUrl;
        this._lat = lat;
        this._lng = lng;
        this.location = new LatLng(lat,lng);
        //this.city = city;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(name);
        parcel.writeString(description);
        parcel.writeString(longDescription);
        parcel.writeParcelable(imageUrl,0);
        parcel.writeParcelable(secondaryImage,0);
        parcel.writeDouble(get_lat());
        parcel.writeDouble(get_lng());
        parcel.writeString(city);
    }

    /**
     * Retrieving Movie data from Parcel object
     * This constructor is invoked by the method createFromParcel(Parcel source) of
     * the object CREATOR
     **/
    private Attraction(Parcel in){
        this.name = in.readString();
        this.description = in.readString();
        this.longDescription = in.readString();
        this.imageUrl = in.readParcelable(null);
        this.secondaryImage = in.readParcelable(null);
        location=new LatLng(in.readDouble(),in.readDouble());
        set_lat(this.location.latitude);
        set_lng(this.location.longitude);
        this.city = in.readString();
    }

    public static final Creator<Attraction> CREATOR = new Creator<Attraction>() {
        @Override
        public Attraction createFromParcel(Parcel source) {
            return new Attraction(source);
        }

        @Override
        public Attraction[] newArray(int size) {
            return new Attraction[size];
        }
    };
}
