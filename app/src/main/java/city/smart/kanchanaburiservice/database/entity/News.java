package city.smart.kanchanaburiservice.database.entity;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class News implements Parcelable {

    @SerializedName("title")
    @Expose
    private String _title;

    @SerializedName("description")
    @Expose
    private String _description;

    @SerializedName("image")
    @Expose
    private String _image;

    public Uri imageUrl;

    public String get_title() {
        return _title;
    }
    public void set_title(String _title) {
        this._title = _title;
    }

    public String get_description() {
        return _description;
    }
    public void set_description(String _description) {
        this._description = _description;
    }

    public String get_image() {
        return _image;
    }
    public void set_image(String _image) {
        this._image = _image;
    }

    public static final Creator<News> CREATOR = new Creator<News>() {
        @Override
        public News createFromParcel(Parcel source) {
            return new News(source);
        }

        @Override
        public News[] newArray(int size) {
            return new News[size];
        }
    };

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(get_title());
        parcel.writeString(get_description());
        parcel.writeParcelable(imageUrl,0);
        parcel.writeString(get_image());
    }
    
    private News(Parcel in){
        set_title(in.readString());
        set_description(in.readString());
        this.imageUrl = in.readParcelable(null);
        set_image(in.readString());
    }

    public  News(String title,String description,String image,Uri imageUrl){
        set_title(title);
        set_description(description);
        set_image(image);
        this.imageUrl = imageUrl;
    }
    @Override
    public int describeContents() {
        return 0;
    }

}
