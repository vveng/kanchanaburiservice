package city.smart.kanchanaburiservice.database.Repository;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import city.smart.kanchanaburiservice.api.ApiWebService;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public abstract class BaseRepository {
    static final String BASE_URL = "http://www.kancity-service.com/api/";
    protected static  String TAG ;
    public ApiWebService getWebservice() {
        return webservice;
    }
    public void setWebservice(ApiWebService webservice) {
        this.webservice = webservice;
    }

    private ApiWebService webservice;
    protected Retrofit retrofit;
    protected Gson gson;

    public void init(){
        this.gson = new GsonBuilder()
                .setLenient()
                .create();
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .build();

        this.retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(gson))
                .baseUrl(BASE_URL)
                .client(client)
                .build();
        setWebservice(retrofit.create(ApiWebService.class));
    }

}
