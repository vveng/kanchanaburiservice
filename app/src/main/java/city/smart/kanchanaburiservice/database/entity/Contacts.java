package city.smart.kanchanaburiservice.database.entity;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Contacts implements Parcelable {
    @SerializedName("name")
    @Expose
    private String _name;
    @SerializedName("phone")
    @Expose
    private String _phone;
    private  String _image;

    public Uri imageUri;

    public String get_name() {
        return _name;
    }
    public void set_name(String _name) {
        this._name = _name;
    }
    public String get_phone() {
        return _phone;
    }
    public void set_phone(String _phone) {
        this._phone = _phone;
    }
    public String get_image() {
        return _image;
    }
    public void set_image(String _image) {
        this._image = _image;
    }

    public Contacts(String name,String phone,Uri imageUri){
        set_name(name);
        set_phone(phone);
        this.imageUri = imageUri;
    }

    private Contacts(Parcel in){
        set_name(in.readString());
        set_phone(in.readString());
    }

    public static final Creator<Contacts> CREATOR = new Creator<Contacts>() {
        @Override
        public Contacts createFromParcel(Parcel source) {
            return new Contacts(source);
        }

        @Override
        public Contacts[] newArray(int size) {
            return new Contacts[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(get_name());
        parcel.writeString(get_phone());
    }
}
