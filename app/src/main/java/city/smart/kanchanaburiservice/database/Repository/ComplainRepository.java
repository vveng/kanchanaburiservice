package city.smart.kanchanaburiservice.database.Repository;

import city.smart.kanchanaburiservice.database.entity.Complain;

import retrofit2.Call;

public class ComplainRepository extends BaseRepository  {

    public Call<Boolean> start(Complain complain) {
        super.init();
        super.TAG =  ComplainRepository.class.getSimpleName();
        return getWebservice().savePost(complain.buildDefaultParams());
    }

}
