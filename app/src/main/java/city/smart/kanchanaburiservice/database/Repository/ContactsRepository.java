package city.smart.kanchanaburiservice.database.Repository;

import android.net.Uri;

import city.smart.kanchanaburiservice.R;
import city.smart.kanchanaburiservice.base.BaseActivity;
import city.smart.kanchanaburiservice.contacts.ContactsActivity;
import city.smart.kanchanaburiservice.database.entity.Contacts;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ContactsRepository extends BaseRepository  implements Callback<List<Contacts>> {
    BaseActivity mActivity;
    ContactsActivity.ContactsAdapter mAdapter;
    public void start(ContactsActivity activity, ContactsActivity.ContactsAdapter adapter){
        super.init();
        super.TAG = ContactsRepository.class.getSimpleName();
        Call<List<Contacts>> call = getWebservice().getContacs(0,0,30);
        this.mActivity = activity;
        this.mAdapter = adapter;
        mActivity.showSnackBar(R.string.loading);
        call.enqueue(this);
    }

    @Override
    public void onResponse(Call<List<Contacts>> call, Response<List<Contacts>> response) {
        if(response.isSuccessful()) {
            //mAdapter.newsList = response.body();
            List<Contacts> listNews = response.body();
            mAdapter.contactList = new ArrayList<>();
            for (final Contacts a : listNews) {
                mAdapter.contactList.add(new Contacts(
                        a.get_name(),
                        a.get_phone(),
                        Uri.parse("http://www.microspot.com/images/subnavcontact.png")
                ));
            }
            mAdapter.notifyDataSetChanged();
        }else
            mActivity.showSnackBar(R.string.error_general);
    }

    @Override
    public void onFailure(Call<List<Contacts>> call, Throwable t) {
        mActivity.showSnackBar(R.string.error_general);
    }
}
