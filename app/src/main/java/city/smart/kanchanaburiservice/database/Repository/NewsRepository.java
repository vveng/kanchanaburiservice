package city.smart.kanchanaburiservice.database.Repository;
import android.net.Uri;
import android.util.Log;

import city.smart.kanchanaburiservice.R;
import city.smart.kanchanaburiservice.base.BaseActivity;
import city.smart.kanchanaburiservice.database.entity.News;
import city.smart.kanchanaburiservice.news.NewsActivity;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NewsRepository extends BaseRepository implements Callback<List<News>> {
    BaseActivity mActivity;
    NewsActivity.NewsAdapter mAdapter;
    public void  start(NewsActivity activity, NewsActivity.NewsAdapter adapter) {
        super.init();
        super.TAG =  ComplainRepository.class.getSimpleName();
         Call<List<News>> call =  getWebservice().getNews(0,0,30);
         this.mActivity = activity;
         this.mAdapter = adapter;
         mActivity.showSnackBar(R.string.loading);
         call.enqueue(this);
    }

    @Override
    public void onResponse(Call<List<News>> call, Response<List<News>> response) {
        if(response.isSuccessful()) {
            //mAdapter.newsList = response.body();
            List<News> listNews = response.body();
            mAdapter.newsList = new ArrayList<>();
            for (final News a : listNews) {
                mAdapter.newsList.add(new News(
                        a.get_title(),
                        a.get_description(),
                        a.get_image(),//long description
                        Uri.parse("http://www.kancity-service.com/images/upload/"+a.get_image())
                ));
            }
            mAdapter.notifyDataSetChanged();
        }else
            mActivity.showSnackBar(R.string.error_general);
    }

    @Override
    public void onFailure(Call<List<News>> call, Throwable t) {
        Log.e(TAG,t.getMessage());
        mActivity.showSnackBar(R.string.error_general);
    }
}
