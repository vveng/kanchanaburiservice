package city.smart.kanchanaburiservice.database.Repository;

import android.net.Uri;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.maps.android.SphericalUtil;
import city.smart.kanchanaburiservice.api.ApiWebService;
import city.smart.kanchanaburiservice.database.entity.Attraction;
import city.smart.kanchanaburiservice.fragment.AttractionListFragment;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static city.smart.kanchanaburiservice.utils.TouristAttractions.TEST_CITY;

//@Singleton
public class AttactionRepository  implements  Callback<List<Attraction>> {
    private static int FRESH_TIMEOUT_IN_MINUTES = 1;
    static final String BASE_URL = "http://www.kancity-service.com/api/";
    private static final String TAG = AttactionRepository.class.getSimpleName();
    private ApiWebService webservice;
    private LatLng curLatLng;
    private String closestCity;
    private List<Attraction> attractions;
    public List<Attraction> getAttractions() {
        return attractions;
    }

    public void setAttractions(List<Attraction> attractions) {
        this.attractions = attractions;
    }

    public LatLng getCurLatLng() {
        return curLatLng;
    }

    public void setCurLatLng(LatLng curLatLng) {
        this.curLatLng = curLatLng;
    }

    public String getClosestCity() {
        return closestCity;
    }

    public void setClosestCity(String closestCity) {
        this.closestCity = closestCity;
    }

    AttractionListFragment.AttractionAdapter mAdapter;
//    private final AttactionDao attactionDao;
//    private final Executor executor;
//    @Inject
    public AttactionRepository(){}
//    @Inject
//    public AttactionRepository(ApiWebService webservice, AttactionDao attactionDao, Executor executor) {
//        this.webservice = webservice;
//        this.attactionDao = attactionDao;
//        this.executor = executor;
//    }

    public void start(AttractionListFragment.AttractionAdapter mAdapter) {
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(gson))
                .baseUrl(BASE_URL)
                .build();
        this.mAdapter = mAdapter;
        this.webservice = retrofit.create(ApiWebService.class);
        Call<List<Attraction>> call = webservice.getAttaction(0,0,30);
        call.enqueue(this);

    }
    @Override
    public void onResponse(Call<List<Attraction>> call, Response<List<Attraction>> response) {

        if(response.isSuccessful()) {

            setAttractions(response.body());
            List<Attraction> listAttractions = new ArrayList<>();

            for (final Attraction a : getAttractions()) {
                listAttractions.add(new Attraction(
                        a.name,
                        a.description,
                        a.description,//long description
                        Uri.parse("http://www.kancity-service.com/images/upload/"+a.get_image()),
                        Uri.parse("http://www.kancity-service.com/images/upload/"+a.get_image()),//secondary image
                        new LatLng(a.get_lat(), a.get_lng()),
                        TEST_CITY
                ));
            }
            setAttractions(listAttractions);

            try {
                mAdapter.mAttractionList = calculateDistance();
                mAdapter.notifyDataSetChanged();
            }catch (Exception e){
                Log.e(TAG, "Errors : " + e.getStackTrace());
            }

        } else {
            Log.e(TAG, "Errors : " + response.errorBody());
        }
    }

    public List<Attraction>  calculateDistance(){

        List<Attraction> listAttractions = new ArrayList<>();

        for (final Attraction a : getAttractions()) {
            listAttractions.add(a);
        }

        if (getCurLatLng() != null) {
            Collections.sort(listAttractions,
                    new Comparator<Attraction>() {
                        @Override
                        public int compare(Attraction lhs, Attraction rhs) {
                            double lhsDistance = SphericalUtil.computeDistanceBetween(
                                    lhs.location, getCurLatLng());
                            double rhsDistance = SphericalUtil.computeDistanceBetween(
                                    rhs.location, getCurLatLng());
                            return (int) (lhsDistance - rhsDistance);
                        }
                    }
            );
        }
        return  listAttractions;
    }
    @Override
    public void onFailure(Call<List<Attraction>> call, Throwable t) {
        Log.e(TAG, "Errors : " + t.getStackTrace());
    }

}