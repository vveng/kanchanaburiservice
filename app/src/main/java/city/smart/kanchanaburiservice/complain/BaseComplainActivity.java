package city.smart.kanchanaburiservice.complain;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import city.smart.kanchanaburiservice.R;
import city.smart.kanchanaburiservice.base.BaseActivity;
import city.smart.kanchanaburiservice.database.Repository.ComplainRepository;
import city.smart.kanchanaburiservice.database.entity.Complain;
import city.smart.kanchanaburiservice.utils.ValidationUtil;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public  class BaseComplainActivity extends BaseActivity implements  Callback<Boolean>{
    protected ImageView imageView;
    protected ProgressBar progressBar;
    protected EditText titleEditText;
    protected EditText descriptionEditText;
    protected EditText phoneEditText;
    protected EditText nameEditText;
    protected Spinner complainSpinner;
    protected boolean creatingPost;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_complain);

        // Handle Toolbar
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(R.string.drawer_item_complaint);

        progressBar = findViewById(R.id.progressBar);

        titleEditText = findViewById(R.id.titleEditText);
        titleEditText.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (titleEditText.hasFocus() && titleEditText.getError() != null) {
                    titleEditText.setError(null);
                    return true;
                }

                return false;
            }
        });

        descriptionEditText = findViewById(R.id.descriptionEditText);
        descriptionEditText.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (descriptionEditText.hasFocus() && descriptionEditText.getError() != null) {
                    descriptionEditText.setError(null);
                    return true;
                }

                return false;
            }
        });

        complainSpinner = findViewById(R.id.complain_spinner);
        complainSpinner.setOnItemSelectedListener(new ItemSelectedListener());

        nameEditText = findViewById(R.id.nameEditText);
        nameEditText.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (nameEditText.hasFocus() && nameEditText.getError() != null) {
                    nameEditText.setError(null);
                    return true;
                }

                return false;
            }
        });

        phoneEditText = findViewById(R.id.phoneEditText);
        phoneEditText.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (phoneEditText.hasFocus() && phoneEditText.getError() != null) {
                    phoneEditText.setError(null);
                    return true;
                }

                return false;
            }
        });

//        imageView = findViewById(R.id.imageView);
//        imageView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                BaseComplainActivity.this.onSelectImageClick(v);
//            }
//        });
    }

    public class ItemSelectedListener implements AdapterView.OnItemSelectedListener {

        //get strings of first item
        String firstItem = String.valueOf(complainSpinner.getSelectedItem());
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
            if (firstItem.equals(String.valueOf(complainSpinner.getSelectedItem()))) {
                // ToDo when first item is selected

            } else {
                // Todo when item is selected by the user
            }
        }


        @Override
        public void onNothingSelected(AdapterView<?> arg) {

        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.create_complain_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.post_complain:
                this.doSavePost();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    protected ImageView getImageView() {
        return imageView;
    }

    protected void attemptCreatePost() {
        // Reset errors.
        this.setTitleError(null);
        this.setDescriptionError(null);
        Complain complain = new Complain();
        complain.set_title(this.getTitleText().trim());
        complain.set_description(this.getDescriptionText().trim());
        complain.set_phone(this.getPhoneText().trim());
        complain.set_name(this.getName().trim());
        complain.set_complainType(this.getComplainType());

        boolean cancel = false;

        if (TextUtils.isEmpty(complain.get_description())) {
            this.setDescriptionError(this.getString(R.string.warning_empty_description));
            cancel = true;
        }

        if (TextUtils.isEmpty(complain.get_title())) {
            this.setTitleError(this.getString(R.string.warning_empty_title));
            cancel = true;
        } else if (!ValidationUtil.isPostTitleValid(complain.get_title())) {
            this.setTitleError(this.getString(R.string.error_post_title_length));
            cancel = true;
        }

        if(TextUtils.isEmpty(complain.get_phone())){
            this.setPhoneError(this.getString(R.string.warning_empty_phone));
            cancel = true;
        }

        if(TextUtils.isEmpty(complain.get_name())){
            this.setNameError(this.getString(R.string.warning_empty_name));
            cancel = true;
        }

        if(complainSpinner.getSelectedItemPosition() == 0){
            this.setComplainTypeError(this.getString(R.string.warning_empty_type));
            cancel = true;
        }

        if (!cancel) {
            creatingPost = true;
            this.hideKeyboard();
            this.savePost(complain);
        }else {
            this.showSnackBar(getSaveFailMessage());
        }

    }

    public void doSavePost() {
        if (!creatingPost) {
            attemptCreatePost();
        }
    }
    public void savePost(Complain complain){
        //do post save
        ComplainRepository complainRepo = new ComplainRepository();
        try {
            Call<Boolean> call = complainRepo.start(complain);
            call.enqueue(this);
        }catch (Exception e){
            Log.e(TAG,e.toString());
        }

    }

    @Override
    public void onResponse(@NonNull Call<Boolean> call, Response<Boolean> response) {
        Log.d(TAG,response.body().toString());
        this.onPostSaved(true);
    }

    @Override
    public void onFailure(@NonNull Call<Boolean> call, Throwable t) {
        Log.e(TAG,t.getMessage());
        this.onPostSaved(false);
    }

    public void onPostSaved(final boolean success) {
        creatingPost = false;

        super.hideProgress();
        if (success) {
            this.showSnackBar(this.getString(R.string.message_post_was_created));
            this.onPostSavedSuccess();
        } else {
            this.showSnackBar(this.getString(R.string.error_general));
        }
    }

    public void setComplainTypeError(String error){
        ((TextView)complainSpinner.getSelectedView()).setError(error);
        complainSpinner.getSelectedView().requestFocus();
    }

    public void setDescriptionError(String error) {
        descriptionEditText.setError(error);
        descriptionEditText.requestFocus();
    }


    public void setTitleError(String error) {
        titleEditText.setError(error);
        titleEditText.requestFocus();
    }


    public void setNameError(String error){
        nameEditText.setError(error);
        nameEditText.requestFocus();
    }


    public void setPhoneError(String error){
        phoneEditText.setError(error);
        phoneEditText.requestFocus();
    }


    public String getTitleText() {
        return titleEditText.getText().toString();
    }


    public String getPhoneText(){
        return phoneEditText.getText().toString();
    }


    public String getComplainType(){
        return String.valueOf(complainSpinner.getSelectedItemPosition());
    }


    public String getName(){
        return nameEditText.getText().toString();
    }

    public String getDescriptionText() {
        return descriptionEditText.getText().toString();
    }


    public void onPostSavedSuccess() {
        setResult(RESULT_OK);
        this.finish();
    }

}
