package city.smart.kanchanaburiservice.news;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import city.smart.kanchanaburiservice.R;
import city.smart.kanchanaburiservice.database.entity.News;
import city.smart.kanchanaburiservice.utils.Constants;

public class NewsFragment extends Fragment {
    private static final String EXTRA_NEWS = "news";
    private News mNews;
    public static NewsFragment createInstance(News news) {
        NewsFragment newsFragment = new NewsFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable(EXTRA_NEWS, news);
        newsFragment.setArguments(bundle);
        return newsFragment;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        View view = inflater.inflate(R.layout.fragment_detail, container, false);
        mNews = getArguments().getParcelable(EXTRA_NEWS);

        if (mNews == null) {
            getActivity().finish();
            return null;
        }

        TextView nameTextView = view.findViewById(R.id.nameTextView);
        TextView descTextView = view.findViewById(R.id.descriptionTextView);
        ImageView imageView = view.findViewById(R.id.imageView);

        final FloatingActionButton mapFab = view.findViewById(R.id.mapFab);
        mapFab.setImageDrawable(getResources().getDrawable(R.drawable.ic_full_sad));


        nameTextView.setText(mNews.get_title());
        descTextView.setText(mNews.get_description());
        int imageSize = getResources().getDimensionPixelSize(R.dimen.image_size)
                * Constants.IMAGE_ANIM_MULTIPLIER;
        Glide.with(this)
                .load(mNews.imageUrl)
                .apply(new RequestOptions()
                        .override(imageSize, imageSize)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .placeholder(R.drawable.empty_photo)
                        .fitCenter()
                )
                .into(imageView);


        mapFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT,
                        mNews.get_title()+ ", " + mNews.get_description());
                sendIntent.setType("text/plain");
                startActivity(sendIntent);
            }
        });
        return view;
    }

}
