package city.smart.kanchanaburiservice.news;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import city.smart.kanchanaburiservice.R;
import city.smart.kanchanaburiservice.base.BaseActivity;
import city.smart.kanchanaburiservice.base.ItemClickListener;
import city.smart.kanchanaburiservice.database.Repository.NewsRepository;
import city.smart.kanchanaburiservice.database.entity.News;
import city.smart.kanchanaburiservice.utils.AttractionsRecyclerView;

import java.util.List;


public class NewsActivity extends BaseActivity {

    protected boolean mItemClicked;
    private int mImageSize;

    protected static NewsAdapter mAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        mImageSize = 240;
        mItemClicked = false;
        try {
        mAdapter = new NewsAdapter(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news);

        //setContentView(R.layout.activity_sample_fragment_dark_toolbar);

        // Handle Toolbar
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(R.string.drawer_item_news);

        AttractionsRecyclerView recyclerView = findViewById(android.R.id.list);
        recyclerView.setEmptyView(findViewById(android.R.id.empty));
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(mAdapter);

        NewsRepository newRepo = new NewsRepository();
        newRepo.start(this,mAdapter);

        }catch (Exception e ){
            Log.e(TAG,e.getMessage());
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        mItemClicked = false;
    }

    public class NewsAdapter  extends RecyclerView.Adapter<ViewHolder>
            implements ItemClickListener {
        public List<News> newsList;
        private Context mContext;

        public NewsAdapter(Context context){
            super();
            mContext = context;
        }

        @NonNull
        @Override
        public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            LayoutInflater inflater = LayoutInflater.from(mContext);
            View view = inflater.inflate(R.layout.list_row, parent, false);
            return new ViewHolder(view, this);
        }

        @Override
        public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
            News news = newsList.get(position);

            holder.mTitleTextView.setText(news.get_title());
            holder.mDescriptionTextView.setText(news.get_description());

            Glide.with(mContext)
                    .load(news.imageUrl)
                    .apply(new RequestOptions()
                            .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                            .placeholder(R.drawable.empty_photo)
                            .override(mImageSize, mImageSize)
                    )
                    .into(holder.mImageView);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public int getItemCount() {
            return newsList == null ? 0 : newsList.size();
        }

        @Override
        public void onItemClick(View view, int position) {
            try{
                if (!mItemClicked) {
                    mItemClicked = true;
                    View heroView = view.findViewById(android.R.id.icon);
                    NewDetailActivity.launch(
                            (NewsActivity) mContext, mAdapter.newsList.get(position), heroView);
                }
            }catch (Exception e){
                Log.e(TAG,e.getStackTrace().toString());
                NewsActivity newAct = (NewsActivity)mContext;
                newAct.showSnackBar(e.getMessage());
            }
        }
    }

    private static class ViewHolder extends RecyclerView.ViewHolder
            implements View.OnClickListener {

        TextView mTitleTextView;
        TextView mDescriptionTextView;
        ImageView mImageView;
        ItemClickListener mItemClickListener;
        TextView mOverlayTextView;

        public ViewHolder(View view, ItemClickListener itemClickListener) {
            super(view);
            mTitleTextView =  view.findViewById(android.R.id.text1);
            mDescriptionTextView = view.findViewById(android.R.id.text2);
            mImageView = view.findViewById(android.R.id.icon);
            mItemClickListener = itemClickListener;
            mOverlayTextView = view.findViewById(R.id.overlaytext);
            mOverlayTextView.setVisibility(View.GONE);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            mItemClickListener.onItemClick(v, getAdapterPosition());
        }
    }
}
