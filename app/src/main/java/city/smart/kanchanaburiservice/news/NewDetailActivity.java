package city.smart.kanchanaburiservice.news;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.view.View;

import city.smart.kanchanaburiservice.R;
import city.smart.kanchanaburiservice.base.BaseActivity;
import city.smart.kanchanaburiservice.database.entity.News;

public class NewDetailActivity extends BaseActivity {
    private static final String EXTRA_NEWS = "news";

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public static void launch(Activity activity, News news, View heroView) {
        try{
            Intent intent = getLaunchIntent(activity, news);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(
                        activity, heroView, heroView.getTransitionName());
                ActivityCompat.startActivity(activity, intent, options.toBundle());
            } else {
                activity.startActivity(intent);
            }
        }catch (Exception e){
            System.out.print(e.getStackTrace());
        }
    }
    public static Intent getLaunchIntent(Context context, News news) {
        Intent intent = new Intent(context, NewDetailActivity.class);
        intent.putExtra(EXTRA_NEWS, news);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sample_dark_toolbar);
        try{
            News news = getIntent().getParcelableExtra(EXTRA_NEWS);
            if (savedInstanceState == null) {
                getSupportFragmentManager().beginTransaction()
                        .add(R.id.container, NewsFragment.createInstance(news))
                        .commit();
            }
        }catch (Exception e){
            System.out.print(e.getStackTrace());
        }
    }
}
