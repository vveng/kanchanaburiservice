package city.smart.kanchanaburiservice.contacts;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import city.smart.kanchanaburiservice.R;
import city.smart.kanchanaburiservice.base.BaseActivity;
import city.smart.kanchanaburiservice.base.ItemClickListener;
import city.smart.kanchanaburiservice.database.Repository.ContactsRepository;
import city.smart.kanchanaburiservice.database.entity.Contacts;
import city.smart.kanchanaburiservice.utils.AttractionsRecyclerView;
import city.smart.kanchanaburiservice.utils.Utils;

import java.util.List;

public class ContactsActivity extends BaseActivity {
    private static final int PERMISSION_REQ = 0;
    protected boolean mItemClicked;
    private int mImageSize;

    protected static ContactsAdapter mAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        mImageSize = 240;
        mItemClicked = false;

        // Check fine location permission has been granted
        if (!Utils.checkFineCallPermission(this)) {
            // See if user has denied permission in the past
            if (ActivityCompat.shouldShowRequestPermissionRationale(
                    this, Manifest.permission.CALL_PHONE)) {
                // Show a simple snackbar explaining the request instead
                showPermissionSnackbar();
            } else {
                // Otherwise request permission from user
                if (savedInstanceState == null) {
                    requestCallPhonePermission();
                }
            }
        }
        // Otherwise permission is granted (which is always the case on pre-M devices)
        fineLocationPermissionGranted();




    }
    /**
     * Run when fine location permission has been granted
     */
    private void fineLocationPermissionGranted() {
        try {
            mAdapter = new ContactsAdapter(this);
            setContentView(R.layout.activity_contacts);

            // Handle Toolbar
            Toolbar toolbar = findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);
            getSupportActionBar().setTitle(R.string.drawer_item_contact);

            AttractionsRecyclerView recyclerView = findViewById(android.R.id.list);
            recyclerView.setEmptyView(findViewById(android.R.id.empty));
            recyclerView.setHasFixedSize(true);
            recyclerView.setAdapter(mAdapter);

            ContactsRepository newRepo = new ContactsRepository();
            newRepo.start(this,mAdapter);

        }catch (Exception e ){
            showSnackBar(e.getMessage());
        }
    }
    @Override
    public void onResume() {
        super.onResume();
        mItemClicked = false;
    }

    /**
     * Show a permission explanation snackbar
     */
    private void showPermissionSnackbar() {
        Snackbar.make(
                findViewById(R.id.container), R.string.permission_explanation, Snackbar.LENGTH_LONG)
                .setAction(R.string.permission_explanation_action, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        requestCallPhonePermission();
                    }
                })
                .show();
    }


    /**
     * Request the phone call permission from the user
     */
    private void requestCallPhonePermission() {
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.CALL_PHONE}, PERMISSION_REQ);
    }

    public class ContactsAdapter  extends RecyclerView.Adapter<ViewHolder>
            implements ItemClickListener {
        public List<Contacts> contactList;
        private Context mContext;

        public ContactsAdapter(Context context){
            super();
            mContext = context;
        }

        @NonNull
        @Override
        public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            LayoutInflater inflater = LayoutInflater.from(mContext);
            View view = inflater.inflate(R.layout.list_contacts, parent, false);
            return new ViewHolder(view, this);
        }

        @Override
        public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
            Contacts contacts = contactList.get(position);

            holder.mTitleTextView.setText(contacts.get_name());
            holder.mDescriptionTextView.setText(contacts.get_phone());
            holder.mImageView.setMaxWidth(mImageSize);
            holder.mImageView.setMaxHeight(mImageSize);
            Glide.with(mContext)
                    .load(contacts.imageUri)
                    .apply(new RequestOptions()
                            .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                            .placeholder(R.drawable.open_on_phone_animation)
                            .override(mImageSize, mImageSize)
                            .circleCrop().optionalCircleCrop()
                    )
                    .into(holder.mImageView);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public int getItemCount() {
            return contactList == null ? 0 : contactList.size();
        }

        @Override
        public void onItemClick(View view, int position) {
            try{
                if (!mItemClicked) {
                    mItemClicked = true;
                    ///Calling number
                    if (Utils.checkFineCallPermission(mContext)) {
                        Intent in = new Intent(Intent.ACTION_CALL, Uri.parse("tel:"+mAdapter.contactList.get(position).get_phone()));
                        try {
                            startActivity(in);
                        } catch (android.content.ActivityNotFoundException ex) {
                            showSnackBar(R.string.error_phone_activity);
                        }
                    }

                }
            }catch (Exception e){
                Log.e(TAG,e.getStackTrace().toString());
                showSnackBar(e.getMessage());
            }
        }
    }

    private static class ViewHolder extends RecyclerView.ViewHolder
            implements View.OnClickListener {

        TextView mTitleTextView;
        TextView mDescriptionTextView;
        ImageView mImageView;
        ItemClickListener mItemClickListener;

        public ViewHolder(View view, ItemClickListener itemClickListener) {
            super(view);
            mTitleTextView =  view.findViewById(android.R.id.text1);
            mDescriptionTextView = view.findViewById(android.R.id.text2);
            mImageView = view.findViewById(android.R.id.icon);
            mItemClickListener = itemClickListener;
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            mItemClickListener.onItemClick(v, getAdapterPosition());
        }
    }
}
