package city.smart.kanchanaburiservice.base;

import android.view.View;

public interface ItemClickListener {
    void onItemClick(View view, int position);
}
